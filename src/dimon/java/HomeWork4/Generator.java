package dimon.java.HomeWork4;

import dimon.java.HomeWork4.model.Bank;
import dimon.java.HomeWork4.model.Vault;
import jdk.internal.org.objectweb.asm.tree.analysis.Value;

/**
 * Created by Dimon on 02.04.18.
 */
public class Generator {
    private Generator() {
    }

    public static Bank[] generate(){
        Bank[] banks = new Bank[3];
        String[] bankName = {"PB", "OB", "PUMB"};
        String[] countries = {"USD", "EUR", "RUR"};
        float[][][] cources = {{{25.5f, 25f},{26f, 25.6f},{0.45f,0.44f}},
                                {{24.8f,24.5f},{30.5f,30f},{0.48f,0.45f}},
                                {{26.3f,25.4f},{32.5f,31f},{0.47f,0.45f}}};
        for (int i = 0; i <bankName.length; i++) {
            Vault[] vaults = new Vault[countries.length];
            for (int j = 0; j < countries.length; j++) {
                for (int k = 0; k < 2; k++) {
                    vaults[j] = new Vault(countries[j],cources[i][j][1],cources[i][j][0]);
                }
            }
            banks[i] = new Bank(bankName[i], vaults);
        }

        return banks;
    }
}
