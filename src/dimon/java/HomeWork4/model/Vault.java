package dimon.java.HomeWork4.model;

/**
 * Created by Dimon on 02.04.18.
 */
public class Vault {
    public String Value;
    public float Buy;
    public float Sell;

    public Vault(String value, float buy, float sell) {
        Value = value;
        Buy = buy;
        Sell = sell;
    }
}
