package dimon.java.HomeWork4;

import dimon.java.HomeWork4.model.Bank;
import dimon.java.HomeWork4.model.Vault;

import java.util.Scanner;

/**
 * Created by Dimon on 02.04.18.
 */
public class Exchange {
    Bank[] banks;
    Scanner in = new Scanner(System.in);
    public Exchange(Bank[] banks) {
        this.banks = banks;
    }

    public void printAll(){
        for (Bank bank : banks) {
            System.out.println("------------");
            System.out.println(bank.nameBank);
            for (Vault vault : bank.vaults) {
                System.out.println("\t"+vault.Value);
                System.out.println("\t\tBuy: "+vault.Buy);
                System.out.println("\t\tSell: "+vault.Sell);
            }
        }
    }

    public int enterHowMuch(){
        System.out.println("Enter how much: ");
        int howMuch = Integer.parseInt(in.nextLine());
        return howMuch;
    }

    public String enterCountry(){
        System.out.println("Enter a vault: ");
            String money = in.nextLine();
                return money.toLowerCase();

        }
    }

