package dimon.java.HomeWork4.model;

/**
 * Created by Dimon on 02.04.18.
 */
public class Bank {
    public String nameBank;
    public Vault[] vaults;

    public Bank(String nameBank, Vault[] vaults) {
        this.nameBank = nameBank;
        this.vaults = vaults;
    }
}
